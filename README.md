# Project Sekai Musics

Project Sekai Musics は凍結され、更新されなくなりました。

プロセカの API の変更により、イベントのランキングや、プレイヤーの楽曲の詳細情報を取得することができなくなりましたので、プレイヤーの完成状況に基づいて楽曲の難易度を判断することはもはや不可能となりました。

譜面生成に関連するツールについては、[Project Sekai Scores](https://gitlab.com/pjsekai/scores) をご覧ください。
